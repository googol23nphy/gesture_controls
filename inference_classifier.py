from collections import deque
def most_common_value(lst):
    if not lst:
        return None

    # Use max with a custom key function to find the most common element
    most_common = max(set(lst), key=lst.count)

    return most_common


import pickle
import ctypes
import cv2
import mediapipe as mp
import numpy as np

import pyautogui as pg
import time
model_dict = pickle.load(open('./model.p', 'rb'))
model = model_dict['model']

cap = cv2.VideoCapture(0)

mp_hands = mp.solutions.hands
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles

hands = mp_hands.Hands(static_image_mode=True, min_detection_confidence=0.3)

labels_dict = {0: 'Open', 1: 'Peace', 2: 'OK', 3: 'Fist', 4: 'Thumb_Up', 5: 'Thumb_Down', 6: 'Pointer'}

last_frames_signs = deque(maxlen=30)
pointer_track = deque(maxlen=30)

start_time = time.time()
frame_counter = 0
fps = 'Unknown'
show_fps = True
is_menu_open = False
tab_sel_open = False
while True:
    try:
        data_aux = []
        x_ = []
        y_ = []

        ret, frame = cap.read()
        frame = cv2.flip(frame,1)
        frame_counter += 1

        H, W, _ = frame.shape

        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        results = hands.process(frame_rgb)
        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    frame,  # image to draw
                    hand_landmarks,  # model output
                    mp_hands.HAND_CONNECTIONS,  # hand connections
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())

                i_tip = hand_landmarks.landmark[8]
                pointer_track.append([int(i_tip.x*W),int(i_tip.y*H)])

                wrist = hand_landmarks.landmark[0]
                for i in range(len(hand_landmarks.landmark)):
                    x = hand_landmarks.landmark[i].x - wrist.x
                    y = hand_landmarks.landmark[i].y - wrist.y
                    z = hand_landmarks.landmark[i].y - wrist.z

                    data_aux.append(x)
                    data_aux.append(y)
                    data_aux.append(z)

                    x_.append(hand_landmarks.landmark[i].x)
                    y_.append(hand_landmarks.landmark[i].y)


            str_prediction = "Unknown"
            if len(data_aux) == 63:
                prediction = model.predict([np.asarray(data_aux)])
                str_prediction = labels_dict[int(prediction[0])]
                last_frames_signs.append(int(prediction[0]))

            x1 = int(min(x_) * W) - 10
            y1 = int(min(y_) * H) - 10

            x2 = int(max(x_) * W) + 10
            y2 = int(max(y_) * H) + 10


            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 4)
            cv2.putText(frame, str_prediction, (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 255, 0), 3,
                        cv2.LINE_AA)


        end_time = time.time()
        elapsed_time = end_time - start_time
        if elapsed_time >= 1:
            fps = frame_counter
            start_time = end_time
            frame_counter = 0

        if show_fps:
            cv2.putText(frame, f"FPS: {fps}", (int(0.1*W), int(0.1*H)), cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 50, 200), 3,
                    cv2.LINE_AA)

        draw_pointer_track = True
        if draw_pointer_track:
            n_of_pts = len(pointer_track)
            for index, point in enumerate(pointer_track):
                intensity = index / n_of_pts
                cv2.circle(frame, (point), int(0.01*H), (0,int(intensity * 255),0),1)


        # print(last_frames_signs)
        list_size = len(list(last_frames_signs))
        if list_size == 30:
            seg_0 = most_common_value(list(last_frames_signs)[0:9])
            seg_1 = most_common_value(list(last_frames_signs)[10:19])
            seg_2 = most_common_value(list(last_frames_signs)[20:29])

            gesture = [seg_0, seg_1, seg_2]


            # if not is_menu_open:
            #     if gesture == [6,0,0] or gesture == [6,6,0]:
            #         print("Open Menu")
            #         pg.keyDown('Alt')
            #         pg.press('Tab')
            #         is_menu_open = True
            #     else:
            #         pg.keyDown('Alt')
            # else:
            #     if gesture == [0,0,3] or gesture == [0,3,3]:
            #         print("Close Menu")
            #         pg.keyUp('Alt')
            #         is_menu_open = False
            #     if most_common_value(gesture) == 4:
            #         pg.press('Right')
            #     if most_common_value(gesture) == 5:
            #         pg.press('Left')





        # Get the screen resolution
        screen_width = ctypes.windll.user32.GetSystemMetrics(0)
        screen_height = ctypes.windll.user32.GetSystemMetrics(1)

        # Calculate the aspect ratio of the image
        aspect_ratio = frame.shape[1] / frame.shape[0]

        # Calculate the new width and height to fit the screen
        new_width = int(screen_width * 0.8)  # You can adjust the scale factor as needed
        new_height = int(new_width / aspect_ratio)
        
        frame = cv2.resize(frame, (new_width, new_height))

        cv2.imshow('frame', frame)
        cv2.waitKey(1)

    except KeyboardInterrupt:
        cap.release()
        cv2.destroyAllWindows()
        break

